// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Snake.generated.h"

class ACPP_SnakeElement;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ACPP_Snake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Snake();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ACPP_SnakeElement> SnakeElementClass;

	UPROPERTY()
	TArray<ACPP_SnakeElement*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	EMovementDirection LastMoveDirection;
	EMovementDirection PlannedMoveDirection;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

	void InteractWithActor(AActor* OtherActor);

	UFUNCTION(BlueprintNativeEvent)
	void Death();
	void Death_Implementation();
};
