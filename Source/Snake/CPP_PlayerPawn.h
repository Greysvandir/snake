// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CPP_PlayerPawn.generated.h"

class UCameraComponent;
class ACPP_Snake;

UCLASS()
class SNAKE_API ACPP_PlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACPP_PlayerPawn();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ACPP_Snake* SnakeRef;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ACPP_Snake>SnakeClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlerPlayerVerticalInput(float Value);

	UFUNCTION()
	void HandlerPlayerHorizontalInput(float Value);
};
