// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_SnakeElement.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "CPP_Snake.h"

// Sets default values
ACPP_SnakeElement::ACPP_SnakeElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

// Called when the game starts or when spawned
void ACPP_SnakeElement::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACPP_SnakeElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_SnakeElement::SetFirstElemType_Implementation()
{
	isHead = true;
	SetOverlapEvent(); //AddDynamic ��������� ����������� � UFUNCTION, ����� �� ��������
}

void ACPP_SnakeElement::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult)
{
	if (isHead)
	{
		if (IsValid(SnakeOwner))
		{
			SnakeOwner->InteractWithActor(OtherActor);
		}
	}
}

void ACPP_SnakeElement::Interact(AActor* InteractInstigator)
{
	if (IsValid(InteractInstigator))
	{
		SnakeOwner->Death();
	}
}

void ACPP_SnakeElement::SetOverlapEvent()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ACPP_SnakeElement::HandleBeginOverlap);
}

