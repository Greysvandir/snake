// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_IInterraction.h"
#include "CPP_SnakeElement.generated.h"

class UStaticMeshComponent;
class ACPP_Snake;

UCLASS()
class SNAKE_API ACPP_SnakeElement : public AActor, public ICPP_IInterraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_SnakeElement();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	bool isHead;
	ACPP_Snake* SnakeOwner;
	FVector OldLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElemType();
	void SetFirstElemType_Implementation();

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult &SweepResult);

	virtual void Interact(AActor* InteractInstigator) override;

	UFUNCTION()
	void SetOverlapEvent();

};
