// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Food.h"
#include "CPP_Snake.h"
#include "CPP_SnakeElement.h"
#include <vector>

// Sets default values
ACPP_Food::ACPP_Food()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

}

// Called when the game starts or when spawned
void ACPP_Food::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_Food::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_Food::Interact(AActor* InteractInstigator)
{
	if (IsValid(InteractInstigator))
	{
		ACPP_Snake* Snake = Cast<ACPP_Snake>(InteractInstigator);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			SpawnNewFood(Snake);
			Destroy();
		}
	}
}

void ACPP_Food::SpawnNewFood(ACPP_Snake* Snake)
{
	std::vector<std::vector<bool>> FieldMatrix(15, std::vector<bool>(15)); // �������������� ������� ����������� ���� �� ����, ����� ��������� ����� ��� ������ ������ �����
	int x, y; //��������������� ���������� �� ������� �� ���� � ������� ������ ���� (�������������)
	FVector ElementLocation;
	for (int i = 0; i < Snake->SnakeElements.Num(); i++)
	{
		ElementLocation = Snake->SnakeElements[i]->GetActorLocation();
		x = FMath::RoundToInt(ElementLocation.X / 60.f) + 7;
		y = FMath::RoundToInt(ElementLocation.Y / 60.f) + 7;
		FieldMatrix[x][y] = true;
		//FString str = "X1=" + FString::FromInt(x) + " Y1=" + FString::FromInt(y);
		//UE_LOG(LogTemp, Warning, TEXT("!!!!!!!!!!!!!!!!!!!!!!!!!!! ���� %s"), *str);
	}
	
	x = FMath::RandRange(0, 14);
	y = FMath::RandRange(0, 14);

	if (FieldMatrix[x][y]) // ���� ����� ������ - ���� ������
	{
		FindEmptyPlace(x, y, FieldMatrix);
	}

	if (x >= 0)
	{
		FVector NewLocation((x - 7) * 60, (y - 7) * 60, 0);
		FTransform NewTransform(NewLocation);
		GetWorld()->SpawnActor<ACPP_Food>(FoodClass, NewTransform);
	}
}

void ACPP_Food::FindEmptyPlace(int& x, int& y, std::vector<std::vector<bool>> FieldMatrix)
{
	int step = 1;
	int stepCount = 2;
	int tx, ty; // ��������� ���������� ��������

	bool eof = false;

	while (!eof)
	{
		tx = x - step;
		ty = y - step;

		eof = true;

		// ������ �� ������� ������� ������ �������� �����. ����� ������� ���������� ���������� � �������, ���� ����� ���������� ��� - ���������� -1 �� x
		// ���� ������ ������ ����� ��� �����, �� ��������� ���� ������� � ��� �� ��� ��� ���� �� ���� �� ��������
		for (int i = 0; i < stepCount; i++)
		{
			tx += 1;
			if (tx >= 0 && tx < 15 && ty >=0 && ty < 15)
			{
				eof = false;
				if (!FieldMatrix[tx][ty])
				{
					x = tx;
					y = ty;
					return;
				}
			}
		}
		for (int i = 0; i < stepCount; i++)
		{
			ty += 1;
			if (tx >= 0 && tx < 15 && ty >= 0 && ty < 15)
			{
				eof = false;
				if (!FieldMatrix[tx][ty])
				{
					x = tx;
					y = ty;
					return;
				}
			}
		}
		for (int i = 0; i < stepCount; i++)
		{
			tx -= 1;
			if (tx >= 0 && tx < 15 && ty >= 0 && ty < 15)
			{
				eof = false;
				if (!FieldMatrix[tx][ty])
				{
					x = tx;
					y = ty;
					return;
				}
			}
		}
		for (int i = 0; i < stepCount; i++)
		{
			ty -= 1;
			if (tx >= 0 && tx < 15 && ty >= 0 && ty < 15)
			{
				eof = false;
				if (!FieldMatrix[tx][ty])
				{
					x = tx;
					y = ty;
					return;
				}
			}
		}

		step++;
		stepCount += 2;
	}

	x = -1; //�� ����� ������� �����
}

