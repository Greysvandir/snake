// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_PlayerPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "CPP_Snake.h"

// Sets default values
ACPP_PlayerPawn::ACPP_PlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void ACPP_PlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void ACPP_PlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACPP_PlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &ACPP_PlayerPawn::HandlerPlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &ACPP_PlayerPawn::HandlerPlayerHorizontalInput);
}

void ACPP_PlayerPawn::CreateSnakeActor()
{
	SnakeRef = GetWorld()->SpawnActor<ACPP_Snake>(SnakeClass, FTransform());
}

void ACPP_PlayerPawn::HandlerPlayerVerticalInput(float Value)
{
	if (IsValid(SnakeRef))
	{
		if (Value > 0 && SnakeRef->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeRef->PlannedMoveDirection = EMovementDirection::UP;
		}
		if (Value < 0 && SnakeRef->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeRef->PlannedMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void ACPP_PlayerPawn::HandlerPlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeRef))
	{
		if (Value > 0 && SnakeRef->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeRef->PlannedMoveDirection = EMovementDirection::RIGHT;
		}
		if (Value < 0 && SnakeRef->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeRef->PlannedMoveDirection = EMovementDirection::LEFT;
		}
	}
}

