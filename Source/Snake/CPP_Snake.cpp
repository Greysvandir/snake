// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Snake.h"
#include "CPP_SnakeElement.h"

// Sets default values
ACPP_Snake::ACPP_Snake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 10.f;
	PlannedMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ACPP_Snake::BeginPlay()
{
	Super::BeginPlay();

	//MovementSpeed = ElementSize;
	SetActorTickInterval(MovementSpeed);

	AddSnakeElement(4);
}

// Called every frame
void ACPP_Snake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ACPP_Snake::AddSnakeElement(int ElementsNum)
{

	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(FVector::ZeroVector);
		if (SnakeElements.Num() == 0)
		{
			NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
		}
		else
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->OldLocation;
		}
		FTransform NewTransform(NewLocation);
		ACPP_SnakeElement* NewSnakeElem = GetWorld()->SpawnActor<ACPP_SnakeElement>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		NewSnakeElem->OldLocation = NewLocation + FVector(ElementSize, 0, 0);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElemType();
		}
	}

	if (SnakeElements.Num() > 25)
	{
		SetActorTickInterval(0.15f);
	}
	else if(SnakeElements.Num() > 20)
	{
		SetActorTickInterval(0.2f);
	}
	else if (SnakeElements.Num() > 15)
	{
		SetActorTickInterval(0.3f);
	}
	else if (SnakeElements.Num() > 10)
	{
		SetActorTickInterval(0.4f);
	}
}

void ACPP_Snake::Move()
{
	FVector MovementVector(FVector::ZeroVector);

	LastMoveDirection = PlannedMoveDirection;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SnakeElements[0]->OldLocation = SnakeElements[0]->GetActorLocation();
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	for (int i = 1; i < SnakeElements.Num(); i++)
	{
		ACPP_SnakeElement* CurrentElement = SnakeElements[i];
		ACPP_SnakeElement* PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->OldLocation;
		CurrentElement->OldLocation = CurrentElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

}

void ACPP_Snake::InteractWithActor(AActor* OtherActor)
{
	ICPP_IInterraction* OtherActorInteractInterface = Cast<ICPP_IInterraction>(OtherActor);
	if (OtherActorInteractInterface)
	{
		OtherActorInteractInterface->Interact(this);
	}
}

void ACPP_Snake::Death_Implementation()
{
	Destroy();
}

