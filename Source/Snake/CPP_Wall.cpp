// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Wall.h"
#include "CPP_Snake.h"

// Sets default values
ACPP_Wall::ACPP_Wall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

}

// Called when the game starts or when spawned
void ACPP_Wall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_Wall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACPP_Wall::Interact(AActor* InteractInstigator)
{
	if (IsValid(InteractInstigator))
	{
		ACPP_Snake* Snake = Cast<ACPP_Snake>(InteractInstigator);
		if (IsValid(Snake))
		{
			Snake->Death();
		}
	}
}

