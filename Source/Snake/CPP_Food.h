// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_IInterraction.h"
#include <vector>
#include "CPP_Food.generated.h"

class UStaticMeshComponent;
class ACPP_Snake;

UCLASS()
class SNAKE_API ACPP_Food : public AActor, public ICPP_IInterraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Food();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ACPP_Food> FoodClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* InteractInstigator) override;

	void SpawnNewFood(ACPP_Snake* Snake);
	void FindEmptyPlace(int& x, int& y, std::vector<std::vector<bool>> FieldMatrix);
};
